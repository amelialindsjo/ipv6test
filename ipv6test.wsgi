import sys

sys.path.insert(0, '/var/www/ipv6test')
activate_this = '/home/ipv6test/virtualenv/ipv6test/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

from ipv6test import app as application
