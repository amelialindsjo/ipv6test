from flask import Flask, render_template, request

app = Flask(__name__)


@app.route('/')
def ipv6test():
    ipv6_address = request.remote_addr
    return render_template('ipv6test.html', ipv6_address=ipv6_address)


@app.route('/traceroute')
def traceroute():
    import scapy
    trace = []
    for i in range (1, 28):
        pkt = scapy.IP(dest=request.remote_addr, ttl=i) / scapy.UDP(dport=33434)
        reply = scapy.sr1(pkt, verbose=0)
        if reply is None:
            break
        elif reply.type == 3:
            trace.append(i + reply.src)
            break
        else:
            trace.append(i + reply.src)

        return trace
